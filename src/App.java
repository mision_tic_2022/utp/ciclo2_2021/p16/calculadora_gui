import javax.swing.*;
import java.awt.event.*;

public class App extends JFrame{
    /**************************
     * Atributos de la ventana
     *************************/    
    private JTextField campo_1;
    private JTextField campo_2;
    private JButton btnSumar;
    private JLabel lblresultado;

    //Constructor
    public App(){
        /*****************************************
         * Configuración de la ventana (JFrame)
         *****************************************/
        this.setTitle("Calculadora");
        this.setBounds(0, 0, 200, 200);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.getContentPane().setLayout(null);

        //Configurar/ubicar los elementos en el JFrame
        this.campo_1 = new JTextField();
        this.campo_1.setBounds(50, 20, 100, 30);
        this.add(this.campo_1);

        this.campo_2 = new JTextField();
        this.campo_2.setBounds(50, 60, 100, 30);
        this.add(this.campo_2);

        this.btnSumar = new JButton();
        this.btnSumar.setText("Sumar");
        this.btnSumar.setBounds(50, 100, 100, 30);
        this.add(this.btnSumar);

        this.lblresultado = new JLabel();
        this.lblresultado.setText("Resultado");
        this.lblresultado.setBounds(50, 140, 100, 30);
        this.add(this.lblresultado);

        //Manejador de eventos para btnSumar
        //Queda a la escucha de un click
        this.btnSumar.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                int num_1 = Integer.parseInt(campo_1.getText());
                int num_2 = Integer.parseInt(campo_2.getText());
                int suma = num_1 + num_2;
                lblresultado.setText("Resultado: "+suma);
            }
        } );
    }

    public static void main(String[] args) throws Exception {
        App calculadora = new App();
        calculadora.setVisible(true);
    }
}
